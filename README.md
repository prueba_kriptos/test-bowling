# Puntaje Bolos

_Proyecto para la lectura e impresión de puntaje de bolos_

## Comenzando

_Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en su máquina local con la finalidad de poder realizar la revisión._
1. Clonar el repositorio en un directorio deseado
```sh
   git clone https://gitlab.com/prueba_kriptos/test-bowling.git
   ```

### Pre-requisitos

_Que cosas que se necesitan para correr el software_

1. Tener instalado IDE Apache Netbeans versión 12 para poder correr la aplicación.
2. Tener instalado java jdk11 en la ruta (C:\Program Files\Java\jdk11).
3. Tener instalado maven (en casos en los que el IDE no incluya por defecto).
4. Tener acceso a internet para descargar las dependencias maven.
### Ejecución del proyecto desde el IDE
1. Abrir el IDE instalado.
2. Abrir el proyecto descargado previamente en la sección **Comenzando**, punto 1.
3. Descargar las dependencias maven que se encuentran en el archivo pom.xml del proyecto.
```
Esto se realiza haciendo clic derecho sobre el proyecto y elegir la opción "build with dependencies".
```
4. Ejecutar el proyecto.
```
Esto se realiza haciendo clic derecho sobre el proyecto y elegir la opción "run".
```
5. En la consola del IDE se mostrará la ejecución de la aplicación, donde se solicitará el path donde se encuentra el archivo, ejemplo: C:\Users\jeffe\Documents\ejemploArchivo.txt

### Ejecución del proyecto desde un archivo ejecutable
1. Descargar el archivo .zip enviado al correo electrónico con nombre **puntajeBolos.zip**
2. Descomprimir el archivo .zip en el directorio deseado, se descomprimirá 3 archivos (start.bat, stop.bat y el jar ejecutable del proyecto).
3. Dar doble clic en el archivo start.bat. 
4. Se abrirá una terminal donde se mostrará la ejecución de la aplicación, donde se solicitará el path donde se encuentra el archivo, ejemplo: C:\Users\jeffe\Documents\ejemploArchivo.txt.


**Nota:** En caso de no tener instalado el jdk11 en el directorio detallado en el punto **Pre-requisitos** en el punto 2, se tendrá que editar el archivo start.bat con un editor de texto y colocar la ruta correcta.


## Autor


* **Jefferson Esteban Yaguana Montero** 


