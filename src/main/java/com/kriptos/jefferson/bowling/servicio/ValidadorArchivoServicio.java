/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio;

/**
 *
 * @author jeffe
 */
public interface ValidadorArchivoServicio {
    byte[] validarArchivo(String directorio);
}
