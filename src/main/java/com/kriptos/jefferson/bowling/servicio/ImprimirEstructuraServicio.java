package com.kriptos.jefferson.bowling.servicio;

import java.util.List;

import com.kriptos.jefferson.bowling.modelo.Player;

public interface ImprimirEstructuraServicio {
	StringBuilder imprimirCuadro(List<Player> players);
}
