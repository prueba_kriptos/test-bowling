/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio.imp;

import com.kriptos.jefferson.bowling.excepciones.InformacionArchivoExcepcion;
import com.kriptos.jefferson.bowling.modelo.Frame;
import com.kriptos.jefferson.bowling.modelo.Player;
import com.kriptos.jefferson.bowling.servicio.ObtenerInformacionArchivoServicio;
import com.kriptos.jefferson.bowling.utilidades.ReglasBolos;
import com.kriptos.jefferson.bowling.utilidades.ValoresDefecto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jeffe
 */
public class ObtenerInformacionArchivoServicioImp implements ObtenerInformacionArchivoServicio{

    @Override
    public Map<String, Player> obtenerJugadores(List<String> lineas) throws InformacionArchivoExcepcion {
        Map<String, Player> players = new HashMap<>();
        String name;
        String score;
        for (String line : lineas) {
            String[] contenido = line.replace("\r", "").split("\t");
            if (contenido != null && contenido.length != 2) {
                throw new InformacionArchivoExcepcion("El contenido de cada linea solo puede tener 2 items, Nombre Jugador y Puntaje");
            }
            name = contenido[0];
            score = contenido[1];
            Player player = players.get(name.toUpperCase());
            if (player == null) {
                player = new Player(name);
                players.put(player.getName().toUpperCase(), player);
                if (players.size() > ReglasBolos.MAX_JUGADORES.value()) {
                    throw new InformacionArchivoExcepcion(
                            "La cantidad de juagadores ha superado a la permitada, la cual es: "
                            + ReglasBolos.MAX_JUGADORES.value());
                }
            }
            player.setFrame(obtenerFramesPorPlayer(player.getFrame(),
                    score.toUpperCase().equals(ValoresDefecto.FAULT) ? String.valueOf(ReglasBolos.FAULT.value())
                    : score));
        }
        return players;
    }

    @Override
    public List<Frame> obtenerFramesPorPlayer(List<Frame> frames, String score) {
        if (frames == null || frames.isEmpty()) {
            frames = new ArrayList<>();
            frames.add(new Frame(ValoresDefecto.FIRST_TURN, score));
        } else {
            int index = frames.size() - 1;
            Frame frame = frames.get(index);
            if (frame.getTurn() < ReglasBolos.MAX_TURNOS.value()) {
                if ((frame.getFirstScore() != null && frame.getSecondScore() != null)
                        || Integer.parseInt(frame.getFirstScore()) == ReglasBolos.MAX_PINOS.value()) {
                    frames.add(new Frame(frame.getTurn() + 1, score));
                } else {
                    if (Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(score) > ReglasBolos.MAX_PINOS
                            .value()) {
                        throw new InformacionArchivoExcepcion(
                                "El total de puntaje del primer tiro y segundo tiro es superior al permitido "
                                + ReglasBolos.MAX_PINOS.value());
                    }
                    frame.setSecondScore(score);
                    frames.set(index, frame);
                }
            } else {
                if (frame.getSecondScore() == null) {
                    frame.setSecondScore(score);
                    frames.set(index, frame);
                } else if (frame.getBonusScore() == null && Integer.valueOf(frame.getFirstScore())
                        + Integer.valueOf(frame.getSecondScore()) >= ReglasBolos.MAX_PINOS.value()) {
                    frame.setBonusScore(score);
                    frames.set(index, frame);
                } else {
                    throw new InformacionArchivoExcepcion(
                            "Se ha excedido el número permitido de turnos " + ReglasBolos.MAX_TURNOS);
                }
            }

        }

        return frames;
    }
    
}
