/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio.imp;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.kriptos.jefferson.bowling.excepciones.LecturaArchivoExcepcion;
import com.kriptos.jefferson.bowling.modelo.Player;
import com.kriptos.jefferson.bowling.servicio.CalcularPuntajeJugadorServicio;
import com.kriptos.jefferson.bowling.servicio.ImprimirEstructuraServicio;
import com.kriptos.jefferson.bowling.servicio.ObtenerInformacionArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.ProcesarArchivoServicio;

/**
 *
 * @author jeffe
 */
public class ProcesarArchivoServicioImp implements ProcesarArchivoServicio {

    @Override
    public List<Player> procesarArchivo(byte[] file) throws LecturaArchivoExcepcion {        
        ImprimirEstructuraServicio servicio = new ImprimirEstructuraServicioImp();
        CalcularPuntajeJugadorServicio calculos = new CalcularPuntajeJugadorServicioImp();    
        ObtenerInformacionArchivoServicio informacion = new ObtenerInformacionArchivoServicioImp();
        List<String> lines = Arrays.asList(new String(file, StandardCharsets.UTF_8).split("\n"));
        Map<String, Player> players = new HashMap<>();
        players = informacion.obtenerJugadores(lines);        
        players.entrySet().forEach(entry -> {
            Player player = entry.getValue();
            player.setFrame(calculos.obtenerPuntajePorFrame(entry.getValue().getFrame()));
            entry.setValue(player);
        });
        System.out.println(servicio.imprimirCuadro(new ArrayList<>(players.values())));
        return players.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

}
