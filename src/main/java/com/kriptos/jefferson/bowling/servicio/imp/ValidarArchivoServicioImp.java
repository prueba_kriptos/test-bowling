/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio.imp;

import com.kriptos.jefferson.bowling.excepciones.LecturaArchivoExcepcion;
import com.kriptos.jefferson.bowling.servicio.ValidadorArchivoServicio;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;

/**
 *
 * @author jeffe
 */
public class ValidarArchivoServicioImp implements ValidadorArchivoServicio {

    @Override
    public byte[] validarArchivo(String directorio) {
        try {
            File file = new File(directorio);
            if(!file.exists()){
                throw new LecturaArchivoExcepcion("El directorio ingresado no existe.");
            }
            byte[]archivo=FileUtils.readFileToByteArray(file); 
            if(new String(archivo,"UTF-8").equals("")){
                throw new LecturaArchivoExcepcion("El archivo se encuentra vacío.");
            }
            InputStream targetStream = new ByteArrayInputStream(archivo);
            BufferedInputStream bis = new BufferedInputStream(targetStream);
            AutoDetectParser parser = new AutoDetectParser();
            Detector detector = parser.getDetector();
            Metadata md = new Metadata();
            md.add(TikaCoreProperties.RESOURCE_NAME_KEY, ".txt");
            MediaType mediaType = detector.detect(bis, md);
            if (!mediaType.toString().equals("text/plain")) {
                throw new LecturaArchivoExcepcion("El archivo cargado no es un archivo de texto");
            }
            return archivo;
        } catch (IOException ex) {
            throw new LecturaArchivoExcepcion("No se puede realizar la lectura del archivo, debido al siguiente error: "+ex.getMessage());
        }
    }

}
