/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio;

import com.kriptos.jefferson.bowling.excepciones.CalculosExcepcion;
import com.kriptos.jefferson.bowling.modelo.Frame;
import java.util.List;

/**
 *
 * @author jeffe
 */
public interface CalcularPuntajeJugadorServicio {
    List<Frame> obtenerPuntajePorFrame(List<Frame> frames) throws CalculosExcepcion;
}
