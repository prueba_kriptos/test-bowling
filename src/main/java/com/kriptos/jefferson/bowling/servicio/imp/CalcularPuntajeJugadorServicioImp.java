/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio.imp;

import com.kriptos.jefferson.bowling.excepciones.CalculosExcepcion;
import com.kriptos.jefferson.bowling.modelo.Frame;
import com.kriptos.jefferson.bowling.servicio.CalcularPuntajeJugadorServicio;
import com.kriptos.jefferson.bowling.utilidades.ReglasBolos;
import java.util.List;

/**
 *
 * @author jeffe
 */
public class CalcularPuntajeJugadorServicioImp implements CalcularPuntajeJugadorServicio{

    @Override
    public List<Frame> obtenerPuntajePorFrame(List<Frame> frames) throws CalculosExcepcion {
        int index = 0;
        int score = 0;
        if (frames == null || frames.isEmpty() || (frames.size() < ReglasBolos.MAX_TURNOS.value() || frames.size() > ReglasBolos.MAX_TURNOS.value())) {
            throw new CalculosExcepcion("El número de turnos de cada jugador no puede ser menor o mayor a "
                    + ReglasBolos.MAX_TURNOS.value());
        }
        for (Frame frame : frames) {

            Frame f1 = frame.getTurn() == ReglasBolos.MAX_TURNOS.value() ? new Frame() : frames.get(index + 1);
            Frame f2 = frame.getTurn() >= ReglasBolos.MAX_TURNOS.value() - 1 ? new Frame() : frames.get(index + 2);

            if (frame.getTurn() == ReglasBolos.MAX_TURNOS.value()) {
                score += Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(frame.getSecondScore())
                        + Integer.parseInt(frame.getBonusScore() == null ? "0" : frame.getBonusScore());
            } else if (Integer.parseInt(frame.getFirstScore()) == ReglasBolos.MAX_PINOS.value()) {
                if (frame.getTurn() >= ReglasBolos.MAX_TURNOS.value() - 1) {
                    score += Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(f1.getFirstScore())
                            + Integer.parseInt(f1.getSecondScore());
                } else if (Integer.parseInt(f1.getFirstScore()) == ReglasBolos.MAX_PINOS.value()) {
                    score += Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(f1.getFirstScore())
                            + Integer.parseInt(f2.getFirstScore());
                } else {
                    score += Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(f1.getFirstScore())
                            + Integer.parseInt(f1.getSecondScore());
                }

            } else if ((Integer.valueOf(frame.getFirstScore())
                    + Integer.valueOf(frame.getSecondScore())) == ReglasBolos.MAX_PINOS.value()) {
                score += (Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(frame.getSecondScore()))
                        + Integer.parseInt(f1.getFirstScore());
            } else {
                score += (Integer.valueOf(frame.getFirstScore()) + Integer.valueOf(frame.getSecondScore()));
            }
            frame.setTotalScore(score);
            index++;
        }

        return frames;
    }
    
}
