package com.kriptos.jefferson.bowling.servicio.imp;

import com.kriptos.jefferson.bowling.modelo.Frame;
import java.util.List;

import com.kriptos.jefferson.bowling.modelo.Player;
import com.kriptos.jefferson.bowling.servicio.ImprimirEstructuraServicio;
import com.kriptos.jefferson.bowling.utilidades.ReglasBolos;

public class ImprimirEstructuraServicioImp implements ImprimirEstructuraServicio {

    public static final String FRAME = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10";
    public static final String PINTFALLS = "\t{VALUE1}\t{VALUE2}";
    public static final String NAME = "{NAME}\n";
    public static final String SCORE = "\t\t{SCORE}";
    public static StringBuilder pintfalls = new StringBuilder();
    public static StringBuilder score = new StringBuilder();

    @Override
    public StringBuilder imprimirCuadro(List<Player> players) {
        StringBuilder finalScore = new StringBuilder();
        finalScore.append(FRAME);
        players.stream().forEach(p -> {
            finalScore.append("\n");
            pintfalls = new StringBuilder();
            score = new StringBuilder();
            finalScore.append(NAME.replace("{NAME}", p.getName()));
            p.getFrame().stream().forEach(f -> {
                pintfalls.append(obtenerPintfalls(f));
                score.append(SCORE.replace("{SCORE}", f.getTotalScore().toString()));
            });
            finalScore.append("Pinfalls").append(pintfalls).append("\n").append("Score").append(score);
        });
        return finalScore;
    }

    private StringBuilder obtenerPintfalls(Frame frame) {
        StringBuilder pintfalls = new StringBuilder();

        if (frame.getTurn() == ReglasBolos.MAX_TURNOS.value()) {
            pintfalls.append(PINTFALLS
                    .replace("{VALUE1}",
                            Integer.parseInt(frame.getFirstScore()) == ReglasBolos.MAX_PINOS.value() ? "X"
                            : frame.getFirstScore())
                    .replace("{VALUE2}", Integer.parseInt(frame.getSecondScore()) == ReglasBolos.MAX_PINOS.value() ? "X"
                            : frame.getSecondScore()));
            pintfalls.append(frame.getBonusScore() != null
                    ? "\t".concat(Integer.parseInt(frame.getBonusScore()) == ReglasBolos.MAX_PINOS.value() ? "X"
                            : frame.getBonusScore())
                    : "");
        } else if (Integer.parseInt(frame.getFirstScore()) == ReglasBolos.MAX_PINOS.value()) {
            pintfalls.append(PINTFALLS.replace("{VALUE1}", "").replace("{VALUE2}", "X"));
        } else if ((Integer.valueOf(frame.getFirstScore())
                + Integer.valueOf(frame.getSecondScore())) == ReglasBolos.MAX_PINOS.value()) {
            pintfalls.append(PINTFALLS.replace("{VALUE1}", frame.getFirstScore()).replace("{VALUE2}", "/"));
        } else {
            pintfalls.append(
                    PINTFALLS.replace("{VALUE1}", frame.getFirstScore()).replace("{VALUE2}", frame.getSecondScore()));
        }

        return pintfalls;

    }

}
