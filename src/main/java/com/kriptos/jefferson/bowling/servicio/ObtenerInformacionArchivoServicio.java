/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.kriptos.jefferson.bowling.servicio;

import com.kriptos.jefferson.bowling.excepciones.InformacionArchivoExcepcion;
import com.kriptos.jefferson.bowling.modelo.Frame;
import com.kriptos.jefferson.bowling.modelo.Player;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jeffe
 */
public interface ObtenerInformacionArchivoServicio {
    Map<String, Player> obtenerJugadores(List<String> lineas) throws InformacionArchivoExcepcion;
    List<Frame> obtenerFramesPorPlayer(List<Frame> frames, String score);
}
