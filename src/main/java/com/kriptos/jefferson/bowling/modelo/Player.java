/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.modelo;

import java.util.List;

import lombok.Data;

/**
 *
 * @author jeffe
 */
@Data
public class Player {

    public Player(String name) {
        this.name = name;
    }    
    

    private String name;
    private List<Frame> frame;
}
