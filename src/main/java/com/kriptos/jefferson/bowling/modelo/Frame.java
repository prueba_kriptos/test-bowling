/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.modelo;

import lombok.Data;

/**
 *
 * @author jeffe
 */
@Data
public class Frame {

    public Frame(Integer turn, String firstScore) {
        this.turn = turn;
        this.firstScore = firstScore;
    }

    public Frame() {
        super();
    }

    private Integer turn;
    private String firstScore;
    private String secondScore;
    private String bonusScore;
    private Integer totalScore;
}
