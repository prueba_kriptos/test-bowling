/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kriptos.jefferson.bowling;

import com.kriptos.jefferson.bowling.excepciones.LecturaArchivoExcepcion;
import com.kriptos.jefferson.bowling.servicio.ProcesarArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.ValidadorArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.imp.ProcesarArchivoServicioImp;
import com.kriptos.jefferson.bowling.servicio.imp.ValidarArchivoServicioImp;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeffe
 */
public class TestBowling {

    private static final Logger LOG = Logger.getLogger(TestBowling.class.getName());

    public static void main(String[] args) {
        TestBowling bowling = new TestBowling();
        bowling.obtenerPuntajeBolos();
    }

    public void obtenerPuntajeBolos() {
        String respuesta = "";
        do {
            try {
                String directorio = "";
                ProcesarArchivoServicio procesar = new ProcesarArchivoServicioImp();
                ValidadorArchivoServicio validacion = new ValidarArchivoServicioImp();
                System.out.println("SISTEMA PARA CALCULOS DE PUNTAJE DE BOLOS");
                System.out.println("===========================================");
                Scanner scan = new Scanner(System.in);
                System.out.println("Ingrese el directorio donde se encuentra el archivo (incluido el nombre del archivo con su extensión): ");
                directorio = scan.nextLine();
                byte[] archivo = validacion.validarArchivo(directorio);
                procesar.procesarArchivo(archivo);
            } catch (LecturaArchivoExcepcion e) {
                LOG.log(Level.SEVERE, e.getLocalizedMessage());
            } finally {
                do {
                    Scanner scan = new Scanner(System.in);
                    System.out.println("Desea cargar otro archivo? (y/n): ");
                    respuesta = scan.nextLine();
                } while (!respuesta.toUpperCase().equals("Y")&&!respuesta.toUpperCase().equals("N"));

            }
        } while (respuesta.toUpperCase().equals("Y"));
        System.out.println("!GRACIAS POR OCUPAR NUESTRO SISTEMA!");

    }

}
