/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.utilidades;

/**
 *
 * @author jeffe
 */
public class Validaciones {
    
    public static boolean validScore(String score){
        return score.matches("^[Ff]|[0-9]+$");
    }
}
