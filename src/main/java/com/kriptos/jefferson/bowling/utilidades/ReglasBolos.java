/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.kriptos.jefferson.bowling.utilidades;

/**
 *
 * @author jeffe
 */
public enum ReglasBolos {
    
    MAX_JUGADORES(5),
    MAX_TURNOS(10),
    MAX_PINOS(10),
    FAULT(0);

    private final int value;

    private ReglasBolos(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
