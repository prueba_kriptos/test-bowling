/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling.excepciones;

/**
 *
 * @author jeffe
 */
public class CalculosExcepcion extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalculosExcepcion() {
    }

    public CalculosExcepcion(String string) {
        super(string);
    }
    
}
