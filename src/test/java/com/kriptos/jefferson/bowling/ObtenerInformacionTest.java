/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling;

import com.kriptos.jefferson.bowling.excepciones.InformacionArchivoExcepcion;
import com.kriptos.jefferson.bowling.servicio.ObtenerInformacionArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.imp.ObtenerInformacionArchivoServicioImp;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author jeffe
 */
public class ObtenerInformacionTest {
    
    private final ObtenerInformacionArchivoServicio informacion = new ObtenerInformacionArchivoServicioImp();
    
    @Test
    public void validarColumnasEnCadaFila() {
        List<String> lineas = Arrays.asList("Jeff\t5\terror");
        Exception exception = Assert.assertThrows(InformacionArchivoExcepcion.class, () -> {
            informacion.obtenerJugadores(lineas);
        });
        Assert.assertEquals("El contenido de cada linea solo puede tener 2 items, Nombre Jugador y Puntaje",exception.getMessage());
    }
    
    @Test
    public void validarNumeroJugadores() {
        List<String> lineas = Arrays.asList("Jeff\t5","Juan\t10","Jesus\t10","Oscar\t10","Alicia\t10","Pedro\t10");
        Exception exception = Assert.assertThrows(InformacionArchivoExcepcion.class, () -> {
            informacion.obtenerJugadores(lineas);
        });
        Assert.assertEquals("La cantidad de juagadores ha superado a la permitada, la cual es: 5",exception.getMessage());
    }
    
    @Test
    public void validarPuntajeUnTurno() {
        List<String> lineas = Arrays.asList("Jeff\t5","Jeff\t10");
        Exception exception = Assert.assertThrows(InformacionArchivoExcepcion.class, () -> {
            informacion.obtenerJugadores(lineas);
        });
        Assert.assertEquals("El total de puntaje del primer tiro y segundo tiro es superior al permitido 10",exception.getMessage());
    }
}
