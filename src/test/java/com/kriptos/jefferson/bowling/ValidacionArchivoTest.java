/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling;

import com.kriptos.jefferson.bowling.excepciones.LecturaArchivoExcepcion;
import com.kriptos.jefferson.bowling.servicio.ValidadorArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.imp.ValidarArchivoServicioImp;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author jeffe
 */
public class ValidacionArchivoTest {

    private final ValidadorArchivoServicio validador = new ValidarArchivoServicioImp();

    @Test
    public void validarArchivoCorrupto() {
        String directorio = "src/main/resources/archivoCorrupto.txt";
        Exception exception = Assert.assertThrows(LecturaArchivoExcepcion.class, () -> {
            validador.validarArchivo(directorio);
        });
        Assert.assertEquals("El archivo cargado no es un archivo de texto",exception.getMessage());
    }
    
    @Test
    public void validarArchivoVacio() {
        String directorio = "src/main/resources/archivoVacio.txt";
        Exception exception = Assert.assertThrows(LecturaArchivoExcepcion.class, () -> {
            validador.validarArchivo(directorio);
        });
        Assert.assertEquals("El archivo se encuentra vacío.",exception.getMessage());
    }
    @Test
    public void validarDirectorioArchivo() {
        String directorio = "src/main/resources/cualquiera/archivoCorrecto.txt";
        Exception exception = Assert.assertThrows(LecturaArchivoExcepcion.class, () -> {
            validador.validarArchivo(directorio);
        });
        Assert.assertEquals("El directorio ingresado no existe.",exception.getMessage());
    }
    
    @Test
    public void validarArchivoCorrecto() {
        String directorio = "src/main/resources/archivoCorrecto.txt";
        Assert.assertNotNull(validador.validarArchivo(directorio));
    }
}
