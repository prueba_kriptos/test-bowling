/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kriptos.jefferson.bowling;

import com.kriptos.jefferson.bowling.modelo.Player;
import com.kriptos.jefferson.bowling.servicio.CalcularPuntajeJugadorServicio;
import com.kriptos.jefferson.bowling.servicio.ImprimirEstructuraServicio;
import com.kriptos.jefferson.bowling.servicio.ObtenerInformacionArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.ValidadorArchivoServicio;
import com.kriptos.jefferson.bowling.servicio.imp.CalcularPuntajeJugadorServicioImp;
import com.kriptos.jefferson.bowling.servicio.imp.ImprimirEstructuraServicioImp;
import com.kriptos.jefferson.bowling.servicio.imp.ObtenerInformacionArchivoServicioImp;
import com.kriptos.jefferson.bowling.servicio.imp.ValidarArchivoServicioImp;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author jeffe
 */
public class ImprimirEstructuraTest {

    private final ObtenerInformacionArchivoServicio informacion = new ObtenerInformacionArchivoServicioImp();
    private final ValidadorArchivoServicio validador = new ValidarArchivoServicioImp();
    private final ImprimirEstructuraServicio imprimir = new ImprimirEstructuraServicioImp();
    private final CalcularPuntajeJugadorServicio calculos = new CalcularPuntajeJugadorServicioImp();

    @Test
    public void validarImpresionEstructura() {
        String directorio = "src/main/resources/archivoCorrecto.txt";
        byte[] archivo = validador.validarArchivo(directorio);
        List<String> lines = Arrays.asList(new String(archivo, StandardCharsets.UTF_8).split("\n"));
        Map<String, Player> players = informacion.obtenerJugadores(lines);
        players.entrySet().forEach(entry -> {
            Player player = entry.getValue();
            player.setFrame(calculos.obtenerPuntajePorFrame(entry.getValue().getFrame()));
            entry.setValue(player);
        });
        String cuadroEsperado = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\nJeff\nPinfalls\t\tX\t7\t/\t9\t0\t\tX\t0\t8\t8\t/\t0\t6\t\tX\t\tX\tX\t8\t1\nScore\t\t20\t\t39\t\t48\t\t66\t\t74\t\t84\t\t90\t\t120\t\t148\t\t167\nJohn\nPinfalls\t3\t/\t6\t3\t\tX\t8\t1\t\tX\t\tX\t9\t0\t7\t/\t4\t4\tX\t9\t0\nScore\t\t16\t\t25\t\t44\t\t53\t\t82\t\t101\t\t110\t\t124\t\t132\t\t151";
        StringBuilder cuadro = imprimir.imprimirCuadro(new ArrayList<>(players.values()));
        Assert.assertEquals(cuadroEsperado, cuadro.toString());
    }
    
    @Test
    public void validarImpresionEstructuraPuntajePerfecto() {
        String directorio = "src/main/resources/puntajePerfecto.txt";
        byte[] archivo = validador.validarArchivo(directorio);
        List<String> lines = Arrays.asList(new String(archivo, StandardCharsets.UTF_8).split("\n"));
        Map<String, Player> players = informacion.obtenerJugadores(lines);
        players.entrySet().forEach(entry -> {
            Player player = entry.getValue();
            player.setFrame(calculos.obtenerPuntajePorFrame(entry.getValue().getFrame()));
            entry.setValue(player);
        });
        String cuadroEsperado = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\nCarl\nPinfalls\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\tX\tX\tX\nScore\t\t30\t\t60\t\t90\t\t120\t\t150\t\t180\t\t210\t\t240\t\t270\t\t300";
        StringBuilder cuadro = imprimir.imprimirCuadro(new ArrayList<>(players.values()));
        Assert.assertEquals(cuadroEsperado, cuadro.toString());
    }
}
